export default JSON.stringify({
    plugins: [
        { moveGroupAttrsToElems: false },
        { removeUselessStrokeAndFill: false },
        { removeComments: true },
        { moveGroupAttrsToElems: false },
        { removeTitle: true },
        { convertPathData: { straightCurves: false } }
    ]
})
