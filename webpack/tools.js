import HappyPack from 'happypack'
import webpack from 'webpack'
import { dir } from 'config'

import CompressionPlugin from 'compression-webpack-plugin'
import ParallelUglifyPlugin from 'webpack-parallel-uglify-plugin'
import HardSourceWebpackPlugin from 'hard-source-webpack-plugin'


export const createHappyPlugin = (id, loaders) => (
    new HappyPack({
        id,
        loaders,
        cache: true,
        verbose: false,
        threads: 4,
        tempDir: '.cache',
        cacheContext: {
            env: process.env.NODE_ENV
        }
    })
)

export const commonMinifyPlugins = [
    new webpack.ContextReplacementPlugin(/moment[\/\\]locale$/, /ru/),
    new CompressionPlugin({
        asset: '[file].gz',
        algorithm: 'gzip',
        regExp: /\.js$|\.html$/,
        threshold: 10240,
        minRatio: 0.8
    }),
    new webpack.DefinePlugin({
        'process.env': {
            REBEM_MOD_DELIM: JSON.stringify('--'),
            NODE_ENV: JSON.stringify(process.env.NODE_ENV)
        }
    }),
    new ParallelUglifyPlugin({
        cacheDir: '.cache',
        uglifyJS: {
            sourceMap: false,
            minimize: true,
            comments: false,
            compress: {
                screw_ie8: true,
                drop_debugger: true,
                warnings: false,
                drop_console: true
            },
            output: {
                comments: false,
                screw_ie8: true,
            }
        }
    }),
    new webpack.optimize.AggressiveMergingPlugin()
]

export const hardCachePlugin = new HardSourceWebpackPlugin({
    cacheDirectory: `${dir}/.cache/[confighash]/`,
    recordsPath: `${dir}/.cache/[confighash]/records.json`,
    configHash() {
        return process.env.NODE_ENV
    },
    environmentPaths: {
        root: process.cwd(),
        directories: ['node_modules'],
        files: ['package.json', 'webpack.config.js', 'webpack/development.js', 'webpack/dll.js', 'webpack/production.js'],
    }
})
