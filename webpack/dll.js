import webpack from 'webpack'
import { dir } from 'config'
import { join } from 'path'
import { commonMinifyPlugins, createHappyPlugin } from './tools'

const happies = [
    createHappyPlugin('js', ['babel?cacheDirectory=true'])
]

const outputPath = `${dir}/dist/`
const name = `[name].${process.env.NODE_ENV}`

const getPlugins = () => {
    return process.env.NODE_ENV === 'production' ? commonMinifyPlugins : []
}

export default {
    context: process.cwd(),
    cache: true,
    entry: {
        vendor: [
            'bowser',
            'babel-polyfill',
            'react',
            'react-dom',
            'react-hot-loader',
            'recompose',
            'autobind-decorator'
        ]
    },
    module: {
        loaders: [{
            exclude: /node_modules/,
            test: /(\.js|\.jsx)$/,
            loader: 'happypack/loader?id=js',
        }]
    },
    output: {
        path: outputPath,
        publicPath: '/layout/js/',
        filename: `${name}.js`,
        library: '[name]',
        pathinfo: true
    },
    plugins: [
        ...happies,
        ...getPlugins(),
        new webpack.DllPlugin({
            name: '[name]',
            path: join(outputPath, `${name}.json`)
        }),
        new webpack.LoaderOptionsPlugin({
            options: {
                plugins: happies
            }
        })

    ]
}
