import webpack from 'webpack'
import { dir } from 'config'
import path from 'path'
import HtmlWebpackPlugin from 'html-webpack-plugin'
import walkSync from '../libs/walkSync'
import { createHappyPlugin } from './tools'

const postCSSPlugins = require('../postcss')()

const happies = [
    createHappyPlugin('js', [`babel-loader?cacheDirectory=${dir}/.cache/${process.env.NODE_ENV}`]),
    createHappyPlugin('styl', ['style-loader', 'css-loader', 'postcss-loader?parser=sugarss']),
    createHappyPlugin('css', ['style-loader', 'css-loader'])
]

const modules = walkSync(path.resolve(`${dir}/src/`), /components/g)

const outputPath = `${dir}/dist/`

const webpackConfig = {
    cache: true,
    entry: {
        app: [
            'react-hot-loader/patch',
            'webpack-dev-server/client?http://localhost:8080',
            'webpack/hot/only-dev-server',
            `${dir}/src/index.jsx`
        ]
    },
    devtool: 'source-map',
    output: {
        path: outputPath,
        publicPath: '/dist/',
        filename: '[name].js',
        pathinfo: true
    },
    resolve: {
        modules: [
            'node_modules',
            path.resolve('./src/'),
            ...modules
        ],
        plugins: [],
        extensions: ['.js', '.jsx', '.styl', '.sss']
    },
    module: {
        noParse: [/moment.js/],
        loaders: [
            {
                test: /(\.js|\.jsx)$/,
                exclude: /node_modules/,
                loader: 'happypack/loader?id=js',
            },
            {
                test: /\.css$/,
                loaders: 'happypack/loader?id=css',
            },
            {
                test: /(\.styl|\.sss)/,
                loader: 'happypack/loader?id=styl',
            },
            {
                test: /\.svg$/,
                loaders: ['url-loader', `svgo-loader?${require('./svgo')}`]
            },
            {
                test: /\.json$/,
                loaders: ['json-loader']
            },
            {
                test: /\.pug$/,
                loaders: ['pug-loader']
            },
            {
                test: /\.(woff|woff2|eot|ttf)$/,
                loader: 'url-loader?limit=100000'
            }
        ]
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: 'src/index.pug',
            inject: false
        }),
        new webpack.NamedModulesPlugin(),
        new webpack.NoErrorsPlugin(),
        new webpack.ContextReplacementPlugin(/moment[/\\]locale$/, /ru/),
        new webpack.DefinePlugin({
            'process.env': {
                REBEM_MOD_DELIM: JSON.stringify('--'),
                NODE_ENV: JSON.stringify(process.env.NODE_ENV)
            }
        }),
        new webpack.LoaderOptionsPlugin({
            options: {
                postcss() {
                    return postCSSPlugins
                },
                plugins: happies
            }
        }),
        // new webpack.DllReferencePlugin({
        //     context: dir,
        //     manifest: require(path.join(outputPath, 'vendor.development.json')),
        // }),
        ...happies
    ]
}

export default webpackConfig
