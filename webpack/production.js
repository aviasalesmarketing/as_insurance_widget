import webpack from 'webpack'
import path from 'path'
import { dir } from 'config'
import walkSync from '../libs/walkSync'
import { commonMinifyPlugins, createHappyPlugin, hardCachePlugin } from './tools'

const postCSSPlugins = require('../postcss')(true)

const happies = [
    createHappyPlugin('js', [`babel-loader?cacheDirectory=${dir}/.cache/${process.env.NODE_ENV}`]),
    createHappyPlugin('styl', ['style-loader', 'css-loader', 'csso-loader', 'postcss-loader?parser=sugarss'])
]
const outputPath = './build/'

export default {
    cache: true,
    devtool: 'cheap-module-source-map',
    entry: {
        app: [
            'babel-polyfill',
            './src/index.jsx'
        ]
    },
    output: {
        path: outputPath,
        publicPath: '/layout/js/',
        filename: '[name].js',
        pathinfo: true
    },
    resolve: {
        extensions: ['.js', '.jsx', '.styl'],
        modules: [
            'node_modules',
            ...walkSync(path.resolve('./src/'), /components/g)
        ]
    },
    module: {
        loaders: [
            {
                exclude: /node_modules/,
                test: /(\.js|\.jsx)$/,
                loader: 'happypack/loader?id=js',
            },
            {
                test: /\.css$/,
                loaders: ['style-loader', 'css-loader', 'csso-loader', 'postcss-loader']
            },
            {
                test: /(\.styl|\.sss)/,
                loader: 'happypack/loader?id=styl',
            },
            {
                test: /\.svg$/,
                loaders: ['url-loader', `svgo-loader?${require('./svgo')}`]
            },
            {
                test: /\.(woff|woff2|eot|ttf)$/,
                loader: 'url-loader?limit=100000'
            },
            {
                test: /\.json$/,
                loaders: ['json-loader']
            },
        ]
    },

    plugins: [
        hardCachePlugin,
        ...happies,
        ...commonMinifyPlugins,
        new webpack.LoaderOptionsPlugin({
            options: {
                postcss() {
                    return postCSSPlugins
                },
                plugins: happies
            }
        }),
        new webpack.optimize.OccurrenceOrderPlugin(),
        // new webpack.DllReferencePlugin({
        //     context: process.cwd(),
        //     manifest: require(path.join(outputPath, 'vendor.production.json')),
        // })
    ]
}
