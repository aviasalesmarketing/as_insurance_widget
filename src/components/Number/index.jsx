import React from 'react'

import './Number.sss'

export default ({ block, mods, name, label, value, onPlus, onMinus }) =>
    <div block='number' mods={mods} mix={{ block, elem: 'number' }}>
        <label
            block='number'
            elem='label'
            htmlFor={name}
        >
            {label}
        </label>
        <div block='number' elem='control'>
            <button
                block='number'
                elem='button'
                mods={{ minus: true }}
                onClick={onMinus}
            />
            <input
                type=''
                block='number'
                elem='input'
                name={name}
                id={name}
                value={value}
                disabled
                readOnly
            />
            <button
                block='number'
                elem='button'
                mods={{ plus: true }}
                onClick={onPlus}
            />
        </div>
    </div>
