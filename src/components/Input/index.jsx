import React, { Component } from 'react'
import InputElement from 'react-input-mask'
import autobind from 'autobind-decorator'

import './Input.sss'

@autobind
export default class Input extends Component {
    state = {
        open: false
    }

    componentDidMount() {
        document.addEventListener('click', this.handle, true)
    }

    componentWillUnmount() {
        document.removeEventListener('click', this.handle, true)
    }

    openAutocomplete(e) {
        this.setState({ open: true })
        this.props.onFocus(e)
    }

    closeAutocomplete() {
        this.setState({ open: false })
    }

    handle(e) {
        const el = this.container
        if (!el.contains(e.target)) {
            this.closeAutocomplete()
        }
    }
    render() {
        const { open } = this.state
        const { block, label, name, value, mask, autocomplete = [], onChange, onSelect, valid } = this.props

        return (
            <div
                block='input'
                mix={{ block, elem: 'input' }}
                mods={{ open, invalid: !valid }}
                ref={ref => (this.container = ref)}
            >
                <InputElement
                    className='input__text'
                    type='text'
                    name={name}
                    id={name}
                    required
                    onChange={onChange}
                    onFocus={this.openAutocomplete}
                    autoComplete='off'
                    value={value}
                    mask={mask}
                    formatChars={{
                        9: '[0-9]',
                        1: '[0-1]',
                        2: '[0-2]',
                        3: '[0-3]',
                    }}
                />
                <label
                    block='input'
                    elem='label'
                    htmlFor={name}
                >
                    {label}
                </label>
                {autocomplete.length > 0 ?
                    <div block='input' elem='autocomplete'>
                        {autocomplete.map(({ code, name }) => (
                            <a
                                href='#'
                                key={code}
                                block='input'
                                elem='link'
                                onClick={onSelect(name)}
                            >
                                {name}
                            </a>
                        ))}
                    </div> : null}
            </div>
        )
    }
}
