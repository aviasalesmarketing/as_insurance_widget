import React from 'react'

import './Header.sss'

export default ({ block }) =>
    <div block='header' mix={{ block, elem: 'header' }}>
        <div block='header' elem='title'>Ваша страховка</div>
        <div block='header' elem='logo' />
    </div>
