class Counter {
    constructor() {
        this.maternal = 'as'
        this.yaCounter = 'yaCounter'
        this.events = []
    }

    setGoal(name, params = {}, category = 'user_behaviour') {
        if (process.env.NODE_ENV !== 'production') {
            console.log('goal', name, params)
            return
        }
        this.yandex(name, params)
        this.google(name, category, params)
        this.mamka(name, params)
    }
    yandex(name, params) {
        if (typeof window[this.yaCounter] !== 'undefined' && window[this.yaCounter] !== null) {
            window[this.yaCounter].reachGoal(name, params)
            if (this.ycallback) {
                this.ycallback(name, params)
            }
        }
    }
    google(name, category, params) {
        if (typeof window.ga !== 'undefined' && window.ga !== null) {
            window.ga('currentProject.send', {
                hitType: 'event',
                eventCategory: category,
                eventAction: name,
                params
            })
            if (Counter.gcallback) {
                Counter.gcallback(name, category, params)
            }
        }
    }
    mamka(name, params) {
        if (typeof window.mamka !== 'undefined' && window.mamka !== null) {
            window.mamka('send_event', {
                name,
                meta: params
            })
        }
    }
}

export default new Counter()
