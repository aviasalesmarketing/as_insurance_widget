import React, { Component } from 'react'
import autobind from 'autobind-decorator'

import format from 'date-fns/format'
import subYears from 'date-fns/sub_years'

import Header from 'components/Header'
import Input from 'components/Input'
import Number from 'components/Number'

import counter from 'libs/events'

import countries from './ingos_countries'

import './Root.sss'

@autobind
export default class Root extends Component {
    state = {
        country: countries.find(this.props.country, true) ? countries.find(this.props.country, true).name : '',
        departure: this.props.departure || '',
        arrival: this.props.arrival || '',
        adults: parseInt(this.props.adults, 10) || 1,
        children: parseInt(this.props.children, 10) || 0,
        infants: parseInt(this.props.infants, 10) || 0,
        valid: false,
        errors: {}
    }

    componentDidMount() {
        this.makeLink()
        this.checkValid()
    }

    onNumberChange(field, changes = 1) {
        return (e) => {
            const result = this.state[field] + changes
            this.changeField(field, result > 0 ? result : field === 'adults' ? 1 : 0)
            e.preventDefault()
        }
    }

    onInputChange(field) {
        return (e) => {
            this.changeField(field, e.target.value, '_change')
        }
    }

    onInputSelect(field) {
        return (value) => {
            return (e) => {
                e.preventDefault()
                this.changeField(field, value, '_list_click')
            }
        }
    }

    onInputFocus(field) {
        return (e) => {
            this.changeField(field, e.target.value, '_click')
        }
    }

    onButtonClick() {
        this.reachGoal('calculate')
    }

    getCompany() {
        switch (this.props.company) {
        case 'absolute':
            return '--absolute'
        case 'strahovkaru':
            return '--strahru'
        case 'ingos':
            return '--ingos'
        default:
            return ''
        }
    }

    changeField(field, value, postfix = '') {
        this.setState({
            [field]: value
        }, () => {
            if (this.checkValid()) this.makeLink()
            this.reachGoal(field, value, postfix)
        })
    }

    reachGoal(field, value, postfix = '') {
        switch (field) {
        case 'adults':
        case 'children':
        case 'infants':
            counter.setGoal(`${field}_${value > 0 ? 'plus' : 'minus'}`, value !== undefined ? { value } : {})
            break
        case 'calculate':
        case 'country':
        case 'departure':
        case 'arrival':
            counter.setGoal(`${field}${postfix}`, value !== undefined ? { value } : {})
            break
        default:
        }
    }

    linkForAbsolute() {
        const { departure, arrival, adults, children, infants } = this.state
        const country = countries.find(this.state.country, true)

        const link = [
            'https://www.absolutins.ru/kupit-strahovoj-polis/puteshestviya/vyezd-za-granicu/?bitrix_include_areas=N&type=single',
            `&datestart=${departure.replace(/\./g, '')}`,
            `&dateend=${arrival.replace(/\./g, '')}`,
            `&country%5b%5d=${country ? country.name : ''}`,
            '&inslimit=50000',
            '&active=y',
            '&documents_loss=y',
            '&accident=y',
            '&baggage_loss=y',
            '&checkout=no',
            '&NEW=Y',
            '&utm_source=aviasales.ru',
            '&utm_medium=widget',
            '&utm_campaign=widget_aviasales.ru'
        ]
        for (let i = 0; i < adults; i += 1) {
            link.push('&tourist%5b%5d=3')
        }
        for (let i = 0; i < children; i += 1) {
            link.push('&tourist%5b%5d=2')
        }
        for (let i = 0; i < infants; i += 1) {
            link.push('&tourist%5b%5d=1')
        }

        return link.join('')
    }

    linkForIngos() {
        const { departure, arrival, adults, children, infants } = this.state

        const country = countries.find(this.state.country, true)

        const years = []
        for (let i = 0; i < adults; i += 1) {
            years.push(30)
        }
        for (let i = 0; i < children; i += 1) {
            years.push(12)
        }
        for (let i = 0; i < infants; i += 1) {
            years.push(2)
        }

        const link = [
            'https://ingos.ru/travel/abroad/calc/country',
            `?=${country ? country.code : ''}`,
            `&datebegin=${departure}`,
            `&dateend=${arrival}`,
            `&years=${years.join(',')}`,
            '&utm_source=aviasales',
            '&utm_medium=widget',
            '&utm_campaign=widget_aviasales.ru',
        ]

        return link.join('')
    }

    linkForStrahovkaRu() {
        const { departure, arrival, adults, children, infants } = this.state

        const reverse = date => date.split('.').join('-')
        const country = countries.find(this.state.country, true)

        const link = [
            'https://strahovkaru.ru/search/travel',
            `?date_from=${reverse(departure)}`,
            '&utm_source=as',
            `&date_to=${reverse(arrival)}`,
            `&countries[]=${country ? country.code : ''}`
        ]


        for (let i = 0; i < adults; i += 1) {
            link.push(`&peoples[]birthday=${format(subYears(new Date(), 32), 'DD-MM-YYYY')}`)
        }
        for (let i = 0; i < children; i += 1) {
            link.push(`&peoples[]birthday=${format(subYears(new Date(), 12), 'DD-MM-YYYY')}`)
        }
        for (let i = 0; i < infants; i += 1) {
            link.push(`&peoples[]birthday=${format(subYears(new Date(), 11), 'DD-MM-YYYY')}`)
        }

        return link.join('')
    }

    makeLink() {
        const { prefix = false } = this.props

        let link
        switch (this.props.company) {
        case 'strahovkaru':
            link = this.linkForStrahovkaRu()
            break
        case 'ingos':
            link = this.linkForIngos()
            break
        default:
            link = this.linkForAbsolute()
        }

        this.setState({ link: prefix ? prefix + encodeURIComponent(link) : link })
    }

    checkValid() {
        let valid = true
        let errors = {}
        const dates = {}
        for (const prop in this.state) {
            switch (prop) {
            case 'country':
                if (!this.state[prop] || this.state[prop] === '' || !countries.find(this.state[prop], true)) {
                    valid = false
                    errors = { ...errors, [prop]: true }
                }
                break
            case 'arrival':
            case 'departure':
                if (!this.state[prop] || this.state[prop] === '') valid = false
                else {
                    const matches = this.state[prop].match(/^(?:(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.]((19|20)[0-9]{2}))$/)
                    if (!matches) {
                        valid = false
                        errors = { ...errors, [prop]: true }
                    } else {
                        dates[prop] = new Date(matches[3], parseInt(matches[2], 10) - 1, matches[1]).getTime()
                        if (dates[prop] < new Date().getTime()) {
                            valid = false
                            errors = { ...errors, [prop]: true }
                        } else if (prop === 'arrival' && !errors.departure && dates[prop] <= dates.departure) {
                            valid = false
                            errors = { ...errors, [prop]: true }
                        }
                    }
                }
                break
            default:
            }
        }
        this.setState({ valid, errors })
        return valid
    }

    render() {
        const { country, departure, arrival, adults, children, infants, valid, link, errors } = this.state
        return (
            <div block={this.getCompany()}>
                <div block='widget'>
                    <Header block='widget' />
                    <div block='widget' elem='content'>
                        <Input
                            name='country'
                            block='widget'
                            label='Страна поездки'
                            onChange={this.onInputChange('country')}
                            onFocus={this.onInputFocus('country')}
                            value={country}
                            valid={!errors.country}
                            autocomplete={countries.find(country, false, true)}
                            onSelect={this.onInputSelect('country')}
                        />
                        <Input
                            name='departure'
                            block='widget'
                            label='Выезжаю'
                            onChange={this.onInputChange('departure')}
                            onFocus={this.onInputFocus('departure')}
                            value={departure}
                            mask='39.19.9999'
                            valid={!errors.departure}
                        />
                        <Input
                            name='arrival'
                            block='widget'
                            label='Возвращаюсь'
                            onChange={this.onInputChange('arrival')}
                            onFocus={this.onInputFocus('arrival')}
                            value={arrival}
                            mask='39.19.9999'
                            valid={!errors.arrival}
                        />
                        <div block='widget' elem='numbers'>
                            <Number
                                name='adults'
                                block='widget'
                                value={adults}
                                label='Взрослые'
                                onPlus={this.onNumberChange('adults')}
                                onMinus={this.onNumberChange('adults', -1)}
                            />
                            <Number
                                name='children'
                                block='widget'
                                value={children}
                                label='Дети до 12 лет'
                                onPlus={this.onNumberChange('children')}
                                onMinus={this.onNumberChange('children', -1)}
                            />
                            <Number
                                name='infants'
                                block='widget'
                                value={infants}
                                label='Дети до 2 лет'
                                onPlus={this.onNumberChange('infants')}
                                onMinus={this.onNumberChange('infants', -1)}
                            />
                        </div>
                        <a
                            block='widget'
                            elem='button'
                            rel='noopener noreferrer'
                            mods={{
                                disabled: !valid
                            }}
                            href={link}
                            onClick={this.onButtonClick}
                            target='_blank'
                        >
                            Рассчитать
                        </a>
                    </div>
                </div>
            </div>
        )
    }
}
