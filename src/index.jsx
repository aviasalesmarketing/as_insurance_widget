import React from 'react'
import { render } from 'react-dom'
import { AppContainer } from 'react-hot-loader'

const renderApp = () => {
    const Root = require('containers/Root')
    const element = document.getElementsByTagName('insurance-widget')[0]
    const props = element.dataset
    render(
        <AppContainer>
            <Root {...props} />
        </AppContainer>,
        element
    )
}
renderApp()
if (module.hot) {
    module.hot.accept('containers/Root', () => {
        renderApp()
    })
}
