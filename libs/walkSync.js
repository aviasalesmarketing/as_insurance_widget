const walkSync = (dir, regexp, dirs = []) => {
    const fs = require('fs')
    const path = require('path')
    const files = fs.readdirSync(dir)

    if (regexp.test(files.join())) dirs.push(dir)

    files.forEach(file => {
        if (fs.statSync(path.join(dir, file)).isDirectory()) {
            dirs = walkSync(path.join(dir, file), regexp, dirs)
        }
    })

    return dirs
}

export default walkSync
