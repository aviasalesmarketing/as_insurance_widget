import { deferConfig as defer } from 'config/defer'

export default {
    root: process.cwd(),
    dir: defer(cfg => (cfg.root)),
}
