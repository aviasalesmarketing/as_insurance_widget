import postcss from 'postcss'


export default postcss.plugin('postcss-rapture', (opts = {}) => {
    opts = {
        overlap: 1,
        cutoffs: [0, 320, 480, 720, 960, 1025, 1366, 1600],
        names: ['no', 'mobile_small', 'mobile_big', 'tablet_small', 'tablet_big', 'desktop_small', 'desktop_big', 'hd'],
        coeffs: {
            mobile_small: 6.4,
            mobile_big: 6.4,
            tablet_small: 16,
            tablet_big: 12,
            desktop_small: 13.6,
            desktop_big: 14,
            hd: 16
        },
        ...opts
    }
    const { overlap, cutoffs, names, coeffs } = opts

    const make = (min = false, max = false) => {
        let value = ''
        if (min) value += `(min-width: ${min}px)`
        if (max) value += ` and (max-width: ${max - overlap}px)`
        return value
    }

    const getValue = (param) => {
        param = param.replace(/(\)|\(|')/g, '')
        return names.indexOf(param)
    }

    const getKey = (name) => {
        return names.reduceRight((previous, current, key) => {
            if (current.indexOf(name) !== -1) {
                previous = key
            }
            return previous
        }, false)
    }

    const getMedia = (name, param) => {
        const preset = getKey(name)
        if (preset) {
            if (names[preset] !== name) return make(cutoffs[preset], cutoffs[preset + 2])
            return make(cutoffs[preset], cutoffs[preset + 1])
        }
        switch (name) {
        case 'below':
            return make(false, cutoffs[getValue(param)])
        case 'above':
            return make(cutoffs[getValue(param)])
        case 'at':
            return param.replace(/\)|\(|"|'/g, '').split(' ').map(el => getParams(el))
        case 'retina':
            return '(-webkit-min-device-pixel-ratio: 2), only screen and (min--moz-device-pixel-ratio: 2), only screen and (-o-min-device-pixel-ratio: 2/1), only screen and (min-device-pixel-ratio: 2), only screen and (min-resolution: 192dpi), only screen and (min-resolution: 2dppx)'
        default:
            return false
        }
    }

    const getCoeff = (name) => {
        const preset = getKey(name)
        if (preset && names[preset] !== name) return (coeffs[names[preset]] + coeffs[names[preset + 1]]) / 2
        return coeffs[name]
    }
    const getParams = (name, params) => {
        const media = getMedia(name, params)
        if (typeof media === 'string') return { name, media, coeff: getCoeff(name) }
        return media
    }

    const processRule = (rule) => {
        rule.each((child) => {
            if (child.type === 'atrule') {
                let params = getParams(child.name, child.params)
                if (params) {
                    if (!Array.isArray(params)) params = [params]

                    params.forEach(({ media, coeff, name }) => {
                        const newChild = child.clone({
                            name: 'media',
                            params: ` only screen and ${media}`
                        })
                        newChild.walkDecls((decl) => {
                            const { prop } = decl
                            let { value } = decl
                            if (value.indexOf('pxvw') === -1) return

                            let match
                            while (match = /pxvw\((-?\d+(.\d)?),?\s?(\d+(.\d)?)?\)/g.exec(value)) {
                                const current = match[3] || coeff
                                const number = parseFloat(match[1])
                                let result = `${Math.round((number / current) * 100) / 100}vw`
                                if (name === names[names.length - 1]) {
                                    result = `${Math.round(number + (number * (current / 100)))}px`
                                }
                                value = value.replace(match[0], result)
                            }
                            decl.cloneBefore({ prop, value })
                            decl.remove()
                        })
                        rule.insertBefore(child, newChild)
                    })
                    child.remove()
                }
            } else if (child.type === 'rule') {
                processRule(child)
            }
        })
    }

    const process = (node) => {
        node.each((child) => {
            if (child.type === 'rule') {
                processRule(child)
            } else if (child.type === 'atrule') {
                process(child)
            }
        })
    }
    return process

})
