// Fork https://github.com/toomuchdesign/postcss-nested-ancestors

import postcss from 'postcss'


export default postcss.plugin('postcss-partial-reference', () => {

    const placeholderRegex = /\^\[(\d+)\]/g

    const replacePlaceholders = parentsStack => (selector) => {
        const match = placeholderRegex.exec(selector)
        return match ? selector.replace(placeholderRegex, parentsStack[match[1]]) : selector
    }

    const process = (node, result, parentsStack = []) => {
        node.each((rule) => {
            const { type, parent } = rule
            if (parent.type === 'root') parentsStack = rule.selector ? [rule.selector] : []
            parentsStack = [...parentsStack]
            if (type === 'rule') {
                if (parent.type === 'rule' && parentsStack.indexOf(parent.selector) === -1) parentsStack.push(parent.selector)
                rule.selectors = rule.selectors.map(replacePlaceholders(parentsStack))
                process(rule, result, parentsStack)
            } else if (type === 'atrule') {
                process(rule, result, parentsStack)
            }
        })
    }
    return process
})
