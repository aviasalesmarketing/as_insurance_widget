// Fork https://github.com/toomuchdesign/postcss-nested-ancestors

import postcss from 'postcss'

const REGEXP = new RegExp(/alpha\((black|white|#[0-9a-f]{3}|#[0-9a-f]{6}), ?(0?\.?\d+)\)/gi)

const hex2rgba = (hex, opacity) => {
    if (hex === 'white') hex = '#ffffff'
    if (hex === 'black') hex = '#000000'
    hex = hex.replace(/^#/, '')
    if (hex.length === 3) hex = `${hex[0]}${hex[0]}${hex[1]}${hex[1]}${hex[2]}${hex[2]}`
    const num = parseInt(hex, 16)
    return `rgba(${num >> 16}, ${num >> 8 & 255}, ${num & 255}, ${parseFloat(opacity).toFixed(2)})`
}

export default postcss.plugin('postcss-alpha', () => (css) => {
    css.replaceValues(REGEXP, { fast: 'alpha(' }, (a, b, c) => {
        return hex2rgba(b, c)
    })
})
