const dir = __dirname

export default (prod = false) => {
    const plugins = [
        //require('postcss-devtools')(),
        require('postcss-initial'),
        require('postcss-mixins'),
        require('postcss-math'),
        require('./postcss-partial-reference'),
        require('./postcss-rapture'),
        require('postcss-nested-ancestors'),
        require('postcss-nested-props'),
        require('postcss-nested'),
        require('postcss-simple-vars'),
        require('./postcss-alpha'),
        require('postcss-short-size')()
    ]
    const extra = prod ? [
        require('postcss-calc-function').default(),
        require('autoprefixer')({ browsers: 'last 4 version' }),
        require('postcss-discard-comments'),
        require('css-mqpacker')({ sort: true })
    ] : []
    return [
        ...plugins,
        ...extra
    ]
}
